import {
    Table,
    Column,
    Model,
    PrimaryKey,
    AutoIncrement,
    IsNumeric,
    NotNull, ForeignKey, BelongsTo, AllowNull,
} from 'sequelize-typescript';
import {Category} from '../category/category.entity';

@Table
export class Product extends Model<Product> {
    @PrimaryKey
    @AutoIncrement
    @IsNumeric
    @AllowNull(false)
    @Column
    id: number;

    @Column
    name: string;

    @ForeignKey(() => Category)
    @AllowNull(false)
    @Column
    categoryId: number;

    @BelongsTo(() => Category)
    category: Category;
}
