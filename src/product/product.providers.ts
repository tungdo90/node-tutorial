
// remove redundant one
import { Product } from './product.entity';

export const productProviders = [
  {
    provide: 'ProductRepository',
    useValue: Product,
  },
];
