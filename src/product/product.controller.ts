import {Controller, Get, Post, Body, Param} from '@nestjs/common';
import { ProductModel } from './model/product.model';
import { ProductService } from './product.service';
import { Product } from './product.entity';
import { Delete, Put } from '@nestjs/common/utils/decorators/request-mapping.decorator';
import {SearchRequest} from '../common/search.model';

@Controller('product')
export class ProductController {
  constructor(private readonly productService: ProductService) {}

  @Post('/create')
  public async create(@Body() productModel: ProductModel) {
    return await this.productService.create(productModel);
  }

  @Get('/all')
  public async findAll(): Promise<Product[]> {
    return await this.productService.findAll();
  }

  @Get('/:id')
  public async findOne(@Param('id') id: number): Promise<Product> {
      return await this.productService.findOne(id);
  }

  @Post('search')
  public async search(@Body() request: SearchRequest) {
      return await this.productService.search(request);
  }

  @Put('/update/:id')
  public async update(@Param('id') id: number, @Body() productModel: ProductModel) {
    return await this.productService.update(id, productModel);
  }

  @Delete('/delete/:id')
  public async delete(@Param('id') id: number) {
    return await this.productService.delete(id);
  }
}
