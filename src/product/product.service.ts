// missing unit test => recommend JEST
// docker, Swagger

import { Injectable, Inject } from '@nestjs/common';
import { ProductModel } from './model/product.model';
import { Product } from './product.entity';
import {Op} from 'sequelize';
import {SearchRequest} from '../common/search.model';

@Injectable()
export class ProductService {
  constructor(
    @Inject('ProductRepository') private readonly productRepository: typeof Product,
  ) {}

  public async create(productModel: ProductModel): Promise<ProductModel> {
    const cat = new Product();
    cat.name = productModel.name;
    cat.categoryId = productModel.categoryId;

    return await cat.save();
  }

  public async update(id, productModel: ProductModel) {
      // should convert to model and pass model to repo
      // only repo reference to sequelize
      const cat = await this.findOne(id);

      return await cat.update(
          {
              name : productModel.name,
              categoryId : productModel.categoryId,
          },
          {
              where: {id},
          },
      );
  }

  public async delete(id: number) {
      return await Product.destroy(
          {
              where: { id },
          },
      );
  }

  public async findAll(): Promise<Product[]> {
    return await this.productRepository.findAll<Product>();
  }

  public async findOne(id: number): Promise<Product> {
      return await this.productRepository.findById(id);
  }

  public async search(request: SearchRequest) {

    const searchString = request.searchString;

    return await this.productRepository.findAndCountAll({
        where: {
            name: {
                [Op.like]: `%${searchString}%`,
            },
        },
    });
  }
}
