import {Controller, Get, Post, Body, Param} from '@nestjs/common';
import {CategoryModel } from './model/category.model';
import { CategoryService } from './category.service';
import { Category } from './category.entity';
import { Delete, Put } from '@nestjs/common/utils/decorators/request-mapping.decorator';
import {SearchRequest} from '../common/search.model';

@Controller('category')
export class CategoryController {
  constructor(private readonly categoryService: CategoryService) {}

  @Post('/create')
  public async create(@Body() categoryModel: CategoryModel) {
    return await this.categoryService.create(categoryModel);
  }

  @Get('/all')
  public async findAll(): Promise<Category[]> {
    return await this.categoryService.findAll();
  }

  @Get('/:id')
  public async findOne(@Param('id') id: number): Promise<Category> {
      return await this.categoryService.findOne(id);
  }

  @Post('search')
  public async search(@Body() request: SearchRequest) {
      return await this.categoryService.search(request);
  }

  @Put('/update/:id')
  public async update(@Param('id') id: number, @Body() categoryModel: CategoryModel) {
    return await this.categoryService.update(id, categoryModel);
  }

  @Delete('/delete/:id')
  public async delete(@Param('id') id: number) {
    return await this.categoryService.delete(id);
  }
}
