import { Injectable, Inject } from '@nestjs/common';
import {CategoryModel} from './model/category.model';
import { Category } from './category.entity';
import {Product} from '../product/product.entity';
import { Op } from 'sequelize';
import {SearchRequest} from '../common/search.model';

@Injectable()
export class CategoryService {
  constructor(
    @Inject('CategoryRepository') private readonly categoryRepository: typeof Category,
  ) {}

  public async create(categoryModel: CategoryModel): Promise<CategoryModel> {
    const cat = new Category();
    cat.name = categoryModel.name;

    return await cat.save();
  }

  public async update(id, categoryModel: CategoryModel): Promise<Category> {
      const cat = await this.findOne(id);

      return await cat.update(
          {
              name : categoryModel.name,
          },
          {
              where: {id},
          },
      );
  }

  public async delete(id: number) {
      return await Product.destroy(
          {
              where: { categoryId: id },
          },
      ).then(
          () => {
              Category.destroy(
                  {
                      where: { id },
                  },
              );
          },
      );
  }

  public async findAll(): Promise<Category[]> {
    return await this.categoryRepository.findAll<Category>();
  }

  public async findOne(id: number): Promise<Category> {
      return await this.categoryRepository.findById(id);
  }

  public async search(request: SearchRequest) {

      const searchString = request.searchString;

      return await this.categoryRepository.findAndCountAll({
          where: {
              name: {
                  [Op.like]: `%${searchString}%`,
              },
          },
      });
  }
}
