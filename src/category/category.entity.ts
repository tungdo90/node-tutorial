import {
    Table,
    Column,
    Model,
    PrimaryKey,
    AutoIncrement,
    IsNumeric,
    NotNull, HasMany, AllowNull,
} from 'sequelize-typescript';
import {Product} from '../product/product.entity';

@Table
export class Category extends Model<Category> {
  @PrimaryKey
  @AutoIncrement
  @IsNumeric
  @AllowNull(false)
  @Column
  id: number;

  @Column
  name: string;

    @HasMany(() => Product)
    players: Product[];
}
