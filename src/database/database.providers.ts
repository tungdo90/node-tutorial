import { Sequelize } from 'sequelize-typescript';
import { Category } from '../category/category.entity';
import { Product } from '../product/product.entity';

export const databaseProviders = [
  {
    provide: 'SequelizeToken',
    useFactory: async () => {
      const sequelize = new Sequelize({
        operatorsAliases: false,
        dialect: 'mysql',
        host: 'localhost',
        port: 3306,
        username: 'root',
        password: '',
        database: 'nest_api',
      });
      sequelize.addModels([Category, Product]);
      await sequelize.sync();
      return sequelize;
    },
  },
];
